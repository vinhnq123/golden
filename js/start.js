(function ($) {
    "use strict"; // Start of use strict

    // Smooth scrolling using jQuery easing
    $('a.js-scroll-trigger[href*="#"]:not([href="#"])').click(function () {
        if (location.pathname.replace(/^\//, '') === this.pathname.replace(/^\//, '') && location.hostname === this.hostname) {
            let target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            if (target.length) {
                $('html, body').animate({
                    scrollTop: (target.offset().top - 54)
                }, 1000, "easeInOutExpo");
                return false;
            }
        }
    });

    // Closes responsive menu when a scroll trigger link is clicked
    $('.js-scroll-trigger').click(function () {
        $('.navbar-collapse').collapse('hide');
    });

    // Activate scrollspy to add active class to navbar items on scroll
    $('body').scrollspy({
        target: '#mainNav',
        offset: 30
    });

    // Collapse Navbar
    let navbarCollapse = function () {
        let navBar = $("#mainNav");
        if (navBar.offset().top > 100) {
            navBar.addClass("navbar-shrink");
        } else {
            navBar.removeClass("navbar-shrink");
        }
    };
    // Collapse now if page is not at top
    navbarCollapse();
    // Collapse the navbar when page is scrolled
    $(window).scroll(navbarCollapse);
    let btnTop = $(".btn-top");
    if (btnTop.length > 0) {
        $(window).scroll(function () {
            let e = $(window).scrollTop();
            if (e > 300) {
                btnTop.show();
                $(".topic-bar").addClass("active");

            } else {
                btnTop.hide();
                $(".topic-bar").removeClass("active");

            }
        });
        btnTop.click(function () {
            $('body,html').animate({
                scrollTop: 0
            })
        });
    }
    $(".navbar-toggler").click(function () {
        if ($("body").hasClass("active")) {
            $("body").removeClass("active");
        } else {
            $("body").addClass("active");
            return false;
        }
    });
    $('.btn-search').on('click',function (){
        $(this).siblings().addClass('active')
    })

    setTimeout(function () {
        $('.loading').fadeOut(0, function () {
            $(this).remove();
        });
    }, 2700);

    $('.btn-search').on('click', function () {
        $(this).parents('.frm-search').find('.txt-search').addClass('active')
    })

    $('.header__dropdown').on('show.bs.dropdown', function () {
        $('.menu-list .nav-item.active').addClass("overlay")
    })
    $('.header__dropdown').on('hide.bs.dropdown', function () {
        $('.menu-list .nav-item.active').removeClass("overlay")
    })

    $(".menu-list .nav-item").on('click', function (event) {
        event.stopPropagation();
        $("#dropdownSearch").dropdown("hide").attr('aria-expanded', 'false');
        $("#dropdownLag").dropdown("hide").attr('aria-expanded', 'false');
        $(this).addClass('active').siblings().removeClass('active')
    })

    $(window).click(function () {
        $(".menu-list .nav-item").removeClass('active')
    });

    if ($('.swiper-intro__item').length)
        new Swiper('.swiper-intro__item', {
            slidesPerView:1,
            navigation: {
                nextEl: '.swiper-intro__item .swiper-button-next',
                prevEl: '.swiper-intro__item .swiper-button-prev',
            },
            autoplay: {
                delay: 2500,
                disableOnInteraction: false,
            },
            pagination: {
                el: '.swiper-intro__item .swiper-pagination',
                clickable: true,
            },
        });

    if ($('.swiper-team').length)
        new Swiper('.swiper-team', {
            navigation: {
                nextEl: '.team__bottom .swiper-button-next',
                prevEl: '.team__bottom .swiper-button-prev',
            },
            pagination: {
                el: '.swiper-pagination',
                clickable: true,
            },
            breakpoints: {
                0: {
                    slidesPerView: 2,
                    spaceBetween: 15,
                },
                576: {
                    slidesPerView: 3,
                    spaceBetween: 15,
                },
                768: {
                    slidesPerView: 4,
                    spaceBetween: 15,
                },
                991: {
                    slidesPerView: 5,
                    spaceBetween: 18,
                },
                1200: {
                    slidesPerView: 6,
                    spaceBetween: 24,
                },
            }
        });


    let swiperServicesThumbPage = $('.swiper-services-thumb-page').length ? new Swiper('.swiper-services-thumb-page', {
        slidesPerView: 6,
        touchRatio: 0
    }) : null

    if ($('.swiper-services-page').length) {
        let swiper = new Swiper('.swiper-services-page', {
            effect: 'coverflow',
            grabCursor: true,
            slidesPerView: 'auto',
            loop:true,
            mousewheel: true,
            coverflowEffect: {
                rotate: 0,
                stretch: 0,
                depth: 500,
                modifier: 1,
            },
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
            },
            thumbs: {
                swiper: swiperServicesThumbPage
            },
            on: {
                init() {
                    $(`.services__left .content[data-index=${this.realIndex}]`).addClass('active').siblings().removeClass('active')
                },
                slideChange() {
                    $(`.services__left .content[data-index=${this.realIndex}]`).addClass('active').siblings().removeClass('active')
                }

            }
        });
    }

    let swiperProjectThumb = $('.swiper-project-top-thumb').length ? new Swiper('.swiper-project-top-thumb', {
        touchRatio: 0
    }) : null

    let topProjectSlide = null;
    if ($('.swiper-project-top').length) {
        topProjectSlide = new Swiper('.swiper-project-top', {
            grabCursor: true,
            autoplay: {
                delay: 3000,
                disableOnInteraction: false,
            },
            pagination: {
                el: '.swiper-project-top ~ .swiper-pagination',
                clickable: true,
            },
            thumbs: {
                swiper: swiperProjectThumb
            }
        });
    }

    $('a[data-toggle="tab"]').on('shown.bs.tab', function (event) {
        if (event.target.id === "nav-top-tab") { // newly activated tab
            if ($('.portfolio-page').length)
                $('.portfolio-page').removeClass('no-bt')
            topProjectSlide = new Swiper('.swiper-project-top', {
                grabCursor: true,
                autoplay: {
                    delay: 3000,
                    disableOnInteraction: false,
                },
                pagination: {
                    el: '.swiper-project-top ~ .swiper-pagination',
                    clickable: true,
                },
                thumbs: {
                    swiper: swiperProjectThumb
                }
            });
        }else if (topProjectSlide) {
            if ($('.portfolio-page').length)
                $('.portfolio-page').addClass('no-bt')
            topProjectSlide.destroy()
        }
    })

    $('.time-line__wrap').on("mouseover", function () {
        if ($(this).attr('data-img').length)
            $('.time-line').css('backgroundImage', `url(${$(this).attr('data-img')})`);
    });
    let dropdownProject = $('.dropdown--project');
    // dropdownProject.on('show.bs.dropdown', function () {
    //     $(this).parent().addClass('overflow')
    //     // do something…
    // })
    //
    // dropdownProject.on('hide.bs.dropdown', function () {
    //     $(this).parent().removeClass('overflow')
    //     // do something…
    // })


})(jQuery); // End of use strict

$(window).on("load", function () {
    $('.loading-page').fadeOut(200, function () {
        $(this).remove();
    });
});

let swiper = new Set();
let count = 0;
let init = false;


/* Which media query
**************************************************************/
function swiperMode() {
    let mobile = window.matchMedia('(min-width: 0px) and (max-width: 768px)');
    let tablet = window.matchMedia('(min-width: 769px) and (max-width: 991px)');
    let desktop = window.matchMedia('(min-width: 991px)');
    let swiperVisionThumb = $('.swiper-vision-thumb').length ? new Swiper('.swiper-vision-thumb', {
        slidesPerView: 3,
        touchRatio: 0
    }) : null

    let swiperServicesThumb = $('.swiper-services-thumb').length ? new Swiper('.swiper-services-thumb', {
        slidesPerView: 6,
        touchRatio: 0
    }) : null
    // Enable (for mobile)
    if (tablet.matches || mobile.matches) {
        if (!init) {
            init = true;
            $('.swiper-services').length ? swiper.add(new Swiper('.swiper-services', {
                spaceBetween: 15,
                pagination: {
                    el: '.swiper-pagination',
                    clickable: true,
                },
                thumbs: {
                    swiper: swiperServicesThumb
                },
            })) : null
            $('.swiper-vision').length ? swiper.add(new Swiper('.swiper-vision', {
                thumbs: {
                    swiper: swiperVisionThumb
                },
                pagination: {
                    el: '.swiper-pagination',
                    clickable: true,
                },
            })) : null
            $('.swiper-personnel').length ? swiper.add(new Swiper('.swiper-personnel', {
                effect: 'coverflow',
                grabCursor: true,
                slidesPerView: 'auto',
                coverflowEffect: {
                    rotate: 0,
                    stretch: 0,
                    depth: 500,
                    modifier: 1,
                },
                pagination: {
                    el: '.swiper-pagination',
                    clickable: true,
                },
            })) : null;
            $('.swiper-brand').length ? swiper.add(new Swiper('.swiper-brand', {
                spaceBetween: 15,
                pagination: {
                    el: '.swiper-pagination',
                    clickable: true,
                },
                breakpoints: {
                    0: {
                        slidesPerView: 3,
                        spaceBetween: 15,
                    },
                    768: {
                        slidesPerView: 6,
                        spaceBetween: 15,
                    },
                    991: {
                        slidesPerView: 8,
                        spaceBetween: 18,
                    },
                    1200: {
                        slidesPerView: 10,
                        spaceBetween: 24,
                    },
                }
            })) : null
            $('.swiper-customer').length ? swiper.add(new Swiper('.swiper-customer', {
                spaceBetween: 15,
                pagination: {
                    el: '.swiper-pagination',
                    clickable: true,
                },
                breakpoints: {
                    0: {
                        slidesPerView: 3,
                        spaceBetween: 15,
                    },
                    768: {
                        slidesPerView: 6,
                        spaceBetween: 15,
                    },
                    991: {
                        slidesPerView: 8,
                        spaceBetween: 18,
                    },
                    1200: {
                        slidesPerView: 10,
                        spaceBetween: 24,
                    },
                }
            })) : null
            // swiper = new Swiper ('.swiper-brand');
        }

    }

        // Disable (for tablet)
        // else if(tablet.matches) {
        //     swiper.destroy();
        //     init = false;
        // }

    // Disable (for desktop)
    else if (desktop.matches) {

        swiper.forEach(value => {
            value.destroy();
            init = false;
        })
        swiper.clear()
    }
}

/* On Load
**************************************************************/
window.addEventListener('load', function () {
    swiperMode();
},);

/* On Resize
**************************************************************/
window.addEventListener('resize', function () {
    swiperMode();
});
